# this is to be saved in the local folder under the name "mpu9250_i2c.py"
# it will be used as the I2C controller and function harbor for the project 
# refer to datasheet and register map for full explanation

import smbus,time

class mpu9250:
    def __init__(self, bus):
        # MPU9250 Registers
        self.MPU9250_ADDR = 0x68
        self.PWR_MGMT_1   = 0x6B
        self.SMPLRT_DIV   = 0x19
        self.CONFIG       = 0x1A
        self.GYRO_CONFIG  = 0x1B
        self.ACCEL_CONFIG = 0x1C
        self.INT_ENABLE   = 0x38
        self.ACCEL_XOUT_H = 0x3B
        self.ACCEL_YOUT_H = 0x3D
        self.ACCEL_ZOUT_H = 0x3F
        self.TEMP_OUT_H   = 0x41
        self.GYRO_XOUT_H  = 0x43
        self.GYRO_YOUT_H  = 0x45
        self.GYRO_ZOUT_H  = 0x47
        #AK8963 registers
        self.AK8963_ADDR   = 0x0C
        self.AK8963_ST1    = 0x02
        self.HXL          = 0x03
        self.HYL          = 0x05
        self.HZL          = 0x07
        self.AK8963_ST2   = 0x09
        self.AK8963_CNTL  = 0x0A
        self.mag_sens = 4900.0 # magnetometer sensitivity: 4800 uT
        self.bus = bus

        self.gyro_config_sel = [0b00000,0b010000,0b10000,0b11000] # byte registers
        self.gyro_config_vals = [250.0,500.0,1000.0,2000.0] # degrees/sec
        self.gyro_indx = 0

        self.accel_config_sel = [0b00000,0b01000,0b10000,0b11000] # byte registers
        self.accel_config_vals = [2.0,4.0,8.0,16.0] # g (g = 9.81 m/s^2)
        self.accel_indx = 0   

    def start(self):
        self.__mpu9250_start()
        self.__AK8963_start()

    def __mpu9250_start(self):
        # alter sample rate (stability)
        self.__mpu9250_write(self.SMPLRT_DIV, 0x00) # sample rate = 8 kHz/(1+samp_rate_div)
        # reset all sensors
        self.__mpu9250_write(self.PWR_MGMT_1, 0x00)
        # power management and crystal settings
        self.__mpu9250_write(self.PWR_MGMT_1, 0x01)
        #Write to Configuration register
        self.__mpu9250_write(self.CONFIG, 0)
        #Write to Gyro configuration register
        self.__mpu9250_write(self.GYRO_CONFIG, int(self.gyro_config_sel[self.gyro_indx]))
        #Write to Accel configuration register
        self.__mpu9250_write(self.ACCEL_CONFIG, int(self.accel_config_sel[self.accel_indx]))
        # interrupt register (related to overflow of data [FIFO])
        self.__mpu9250_write(self.INT_ENABLE, 1)

    def mpu9250_conv(self):
        # raw acceleration bits
        acc_x = self.__mpu9250_read(self.ACCEL_XOUT_H)
        acc_y = self.__mpu9250_read(self.ACCEL_YOUT_H)
        acc_z = self.__mpu9250_read(self.ACCEL_ZOUT_H)

        # raw temp bits
    ##    t_val = read_raw_bits(TEMP_OUT_H) # uncomment to read temp
        
        # raw gyroscope bits
        gyro_x = self.__mpu9250_read(self.GYRO_XOUT_H)
        gyro_y = self.__mpu9250_read(self.GYRO_YOUT_H)
        gyro_z = self.__mpu9250_read(self.GYRO_ZOUT_H)

        #convert to acceleration in g and gyro dps
        a_x = (acc_x/(2.0**15.0))*self.accel_config_vals[self.accel_indx]
        a_y = (acc_y/(2.0**15.0))*self.accel_config_vals[self.accel_indx]
        a_z = (acc_z/(2.0**15.0))*self.accel_config_vals[self.accel_indx]

        w_x = (gyro_x/(2.0**15.0))*self.gyro_config_vals[self.gyro_indx]
        w_y = (gyro_y/(2.0**15.0))*self.gyro_config_vals[self.gyro_indx]
        w_z = (gyro_z/(2.0**15.0))*self.gyro_config_vals[self.gyro_indx]

    ##    temp = ((t_val)/333.87)+21.0 # uncomment and add below in return
        return a_x,a_y,a_z,w_x,w_y,w_z

    def __mpu9250_write(self, register, value):
        self.__write(self.MPU9250_ADDR, register, value)

    def __mpu9250_read(self, register):
        return self.__read_raw_bits(self.MPU9250_ADDR, register)

    def __AK8963_start(self):
        self.__AK8963_write(self.AK8963_CNTL,0x00)
        AK8963_bit_res = 0b0001 # 0b0001 = 16-bit
        AK8963_samp_rate = 0b0110 # 0b0010 = 8 Hz, 0b0110 = 100 Hz
        AK8963_mode = (AK8963_bit_res <<4)+AK8963_samp_rate # bit conversion
        self.__AK8963_write(self.AK8963_CNTL,AK8963_mode)
    
    def AK8963_conv(self):
        # raw magnetometer bits

        loop_count = 0
        while loop_count < 10:
            mag_x = self.__AK8963_read(self.HXL)
            mag_y = self.__AK8963_read(self.HYL)
            mag_z = self.__AK8963_read(self.HZL)

            # the next line is needed for AK8963
            if bin(self.bus.read_byte_data(self.AK8963_ADDR, self.AK8963_ST2))=='0b10000':
                break
            loop_count+=1
            
        #convert to acceleration in g and gyro dps
        m_x = (mag_x/(2.0**15.0))*self.mag_sens
        m_y = (mag_y/(2.0**15.0))*self.mag_sens
        m_z = (mag_z/(2.0**15.0))*self.mag_sens

        return m_x,m_y,m_z


    def __AK8963_write(self, register, value):
        self.__write(self.AK8963_ADDR, register, value)

    def __AK8963_read(self, register):
        return self.__read_raw_bits(self.AK8963_ADDR, register)

    def __write(self, addr, register, value):
        self.bus.write_byte_data(addr, register, value)
        time.sleep(0.1)

    def __read_raw_bits(self, addr, register):
        # read values
        high = self.bus.read_byte_data(addr, register)
        low = self.bus.read_byte_data(addr, register+1)

        # combine higha and low for unsigned bit value
        value = ((high << 8) | low)
        
        # convert to +- value
        if(value > 32768):
            value -= 65536
        return value
