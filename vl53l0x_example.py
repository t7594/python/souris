import smbus, time
from vl53l0x import vl53l0x

bus = smbus.SMBus(1) # start comm with i2c bus
vlx = vl53l0x(bus)
vlx.start()
time.sleep(1) 


while(1):
    dist = vlx.read_range_single()

    print("dist : " + str(dist))
    time.sleep(1)
