import smbus,time
from enum import Flag, auto

class vl53l0x:
    def __init__(self, bus):
        # vl53l0x Registers
        self.VL53L0X_ADDR_DEFAULT = 0x29
        self.REG_IDENTIFICATION_MODEL_ID = 0xC0
        self.REG_VHV_CONFIG_PAD_SCL_SDA_EXTSUP_HV = 0x89
        self.REG_MSRC_CONFIG_CONTROL = 0x60
        self.REG_FINAL_RANGE_CONFIG_MIN_COUNT_RATE_RTN_LIMIT = 0x44
        self.REG_SYSTEM_SEQUENCE_CONFIG = 0x01
        self.REG_DYNAMIC_SPAD_REF_EN_START_OFFSET = 0x4F
        self.REG_DYNAMIC_SPAD_NUM_REQUESTED_REF_SPAD = 0x4E
        self.REG_GLOBAL_CONFIG_REF_EN_START_SELECT = 0xB6
        self.REG_SYSTEM_INTERRUPT_CONFIG_GPIO = 0x0A
        self.REG_GPIO_HV_MUX_ACTIVE_HIGH = 0x84
        self.REG_SYSTEM_INTERRUPT_CLEAR = 0x0B
        self.REG_RESULT_INTERRUPT_STATUS = 0x13
        self.REG_SYSRANGE_START = 0x00
        self.REG_GLOBAL_CONFIG_SPAD_ENABLES_REF_0 = 0xB0
        self.REG_RESULT_RANGE_STATUS = 0x14

        self.RANGE_SEQUENCE_STEP_TCC = 0x10 # Target CentreCheck 
        self.RANGE_SEQUENCE_STEP_MSRC = 0x04 # Minimum Signal Rate Check
        self.RANGE_SEQUENCE_STEP_DSS = 0x28 # Dynamic SPAD selection 
        self.RANGE_SEQUENCE_STEP_PRE_RANGE = 0x40
        self.RANGE_SEQUENCE_STEP_FINAL_RANGE = 0x80

        self.stop_variable = False

        self.bus = bus

    def start(self):

        if not self.__device_is_booted() :
            return False

        # Set 2v8 mode
        vhv_config_scl_sda = self.__read(self.REG_VHV_CONFIG_PAD_SCL_SDA_EXTSUP_HV)
        vhv_config_scl_sda |= 0x01
        self.__write(self.REG_VHV_CONFIG_PAD_SCL_SDA_EXTSUP_HV, vhv_config_scl_sda)

        # Set I2C standard mode
        self.__write(0x88, 0x00)
        self.__write(0x80, 0x01)
        self.__write(0xFF, 0x01)
        self.__write(0x00, 0x00)

        self.stop_variable = self.__read(0x91)

        self.__write(0x00, 0x01)
        self.__write(0xFF, 0x00)
        self.__write(0x80, 0x00)

        # Set init default tuning
        self.__init_default_tuning()

        # Desactivate interrupt option
        self.__configure_interrupt()

        # Activate sequence steps
        self.__write(self.REG_SYSTEM_SEQUENCE_CONFIG,
                        self.RANGE_SEQUENCE_STEP_DSS +
                        self.RANGE_SEQUENCE_STEP_PRE_RANGE +
                        self.RANGE_SEQUENCE_STEP_FINAL_RANGE)

        #
        self.__perform_ref_calibration()

        return True


    def read_range_single(self) :

        self.__write(0x80, 0x01)
        self.__write(0xFF, 0x01)
        self.__write(0x00, 0x00)
        self.__write(0x91, self.stop_variable)
        self.__write(0x00, 0x01)
        self.__write(0xFF, 0x00)
        self.__write(0x80, 0x00)

        self.__write(self.REG_SYSRANGE_START, 0x01)

        sysrange_start = self.__read(self.REG_SYSRANGE_START)
        while (sysrange_start & 0x01) :
            sysrange_start = self.__read(self.REG_SYSRANGE_START)
            time.sleep(0.1)

        interrupt_status = self.__read(self.REG_RESULT_INTERRUPT_STATUS)
        while ((interrupt_status & 0x07) == 0) :
            interrupt_status = self.__read(self.REG_RESULT_INTERRUPT_STATUS)
            time.sleep(0.1)

        

        dist = self.__read(self.REG_RESULT_RANGE_STATUS + 10) 

        self.__write(self.REG_SYSTEM_INTERRUPT_CLEAR, 0x01)

        # 8190 or 8191 may be returned when obstacle is out of range.

        if dist == 8190 or dist == 8191 :
            dist = -1

        return dist


    # We can read the model id to confirm that the device is booted.
    # (There is no fresh_out_of_reset as on the vl6180x)
    def __device_is_booted(self):
        device_id = self.__read(self.REG_IDENTIFICATION_MODEL_ID)

        return device_id == 0xEE # Expected_device_id



    def __init_default_tuning(self): 
        #init not explane by ST...
        self.__write(0xFF, 0x01)
        self.__write(0x00, 0x00)
        self.__write(0xFF, 0x00)
        self.__write(0x09, 0x00)
        self.__write(0x10, 0x00)
        self.__write(0x11, 0x00)
        self.__write(0x24, 0x01)
        self.__write(0x25, 0xFF)
        self.__write(0x75, 0x00)
        self.__write(0xFF, 0x01)
        self.__write(0x4E, 0x2C)
        self.__write(0x48, 0x00)
        self.__write(0x30, 0x20)
        self.__write(0xFF, 0x00)
        self.__write(0x30, 0x09)
        self.__write(0x54, 0x00)
        self.__write(0x31, 0x04)
        self.__write(0x32, 0x03)
        self.__write(0x40, 0x83)
        self.__write(0x46, 0x25)
        self.__write(0x60, 0x00)
        self.__write(0x27, 0x00)
        self.__write(0x50, 0x06)
        self.__write(0x51, 0x00)
        self.__write(0x52, 0x96)
        self.__write(0x56, 0x08)
        self.__write(0x57, 0x30)
        self.__write(0x61, 0x00)
        self.__write(0x62, 0x00)
        self.__write(0x64, 0x00)
        self.__write(0x65, 0x00)
        self.__write(0x66, 0xA0)
        self.__write(0xFF, 0x01)
        self.__write(0x22, 0x32)
        self.__write(0x47, 0x14)
        self.__write(0x49, 0xFF)
        self.__write(0x4A, 0x00)
        self.__write(0xFF, 0x00)
        self.__write(0x7A, 0x0A)
        self.__write(0x7B, 0x00)
        self.__write(0x78, 0x21)
        self.__write(0xFF, 0x01)
        self.__write(0x23, 0x34)
        self.__write(0x42, 0x00)
        self.__write(0x44, 0xFF)
        self.__write(0x45, 0x26)
        self.__write(0x46, 0x05)
        self.__write(0x40, 0x40)
        self.__write(0x0E, 0x06)
        self.__write(0x20, 0x1A)
        self.__write(0x43, 0x40)
        self.__write(0xFF, 0x00)
        self.__write(0x34, 0x03)
        self.__write(0x35, 0x44)
        self.__write(0xFF, 0x01)
        self.__write(0x31, 0x04)
        self.__write(0x4B, 0x09)
        self.__write(0x4C, 0x05)
        self.__write(0x4D, 0x04)
        self.__write(0xFF, 0x00)
        self.__write(0x44, 0x00)
        self.__write(0x45, 0x20)
        self.__write(0x47, 0x08)
        self.__write(0x48, 0x28)
        self.__write(0x67, 0x00)
        self.__write(0x70, 0x04)
        self.__write(0x71, 0x01)
        self.__write(0x72, 0xFE)
        self.__write(0x76, 0x00)
        self.__write(0x77, 0x00)
        self.__write(0xFF, 0x01)
        self.__write(0x0D, 0x01)
        self.__write(0xFF, 0x00)
        self.__write(0x80, 0x01)
        self.__write(0x01, 0xF8)
        self.__write(0xFF, 0x01)
        self.__write(0x8E, 0x01)
        self.__write(0x00, 0x01)
        self.__write(0xFF, 0x00)
        self.__write(0x80, 0x00)

    def __configure_interrupt(self):
        # Interrupt on new sample ready
        self.__write(self.REG_SYSTEM_INTERRUPT_CONFIG_GPIO, 0x04)

        # Configure active low since the pin is pulled-up on most breakout boards
        gpio_hv_mux_active_high = self.__read(self.REG_GPIO_HV_MUX_ACTIVE_HIGH)
        gpio_hv_mux_active_high &= ~0x10
        self.__write(self.REG_GPIO_HV_MUX_ACTIVE_HIGH, gpio_hv_mux_active_high)

        self.__write(self.REG_SYSTEM_INTERRUPT_CLEAR, 0x01)

    class calibration_type_t(Flag):
        CALIBRATION_TYPE_VHV = auto()
        CALIBRATION_TYPE_PHASE = auto()

    def __perform_single_ref_calibration(self, calib_type):
        sysrange_start = 0
        sequence_config = 0

        if(calib_type == 0) : #CALIBRATION_TYPE_VHV
            sequence_config = 0x01
            sysrange_start = 0x01 | 0x40
        elif(calib_type == 1) : # CALIBRATION_TYPE_PHASE
            sequence_config = 0x02
            sysrange_start = 0x01 | 0x00


        self.__write(self.REG_SYSTEM_SEQUENCE_CONFIG, sequence_config)
        self.__write(self.REG_SYSRANGE_START, sysrange_start)

        # Wait for interrupt
        interrupt_status = self.__read(self.REG_RESULT_INTERRUPT_STATUS)

        while ((interrupt_status & 0x07) == 0):
            interrupt_status = self.__read(self.REG_RESULT_INTERRUPT_STATUS)
            time.sleep(0.1)

        self.__write(self.REG_SYSTEM_INTERRUPT_CLEAR, 0x01)

        self.__write(self.REG_SYSRANGE_START, 0x00)


        # Temperature calibration needs to be run again if the temperature changes by
        # more than 8 degrees according to the datasheet.

    def __perform_ref_calibration(self):
        self.__perform_single_ref_calibration(0)
        self.__perform_single_ref_calibration(1)

        # Activate sequence steps
        self.__write(self.REG_SYSTEM_SEQUENCE_CONFIG,
                        self.RANGE_SEQUENCE_STEP_DSS +
                        self.RANGE_SEQUENCE_STEP_PRE_RANGE +
                        self.RANGE_SEQUENCE_STEP_FINAL_RANGE)


    def __write(self, register, value):
        self.bus.write_byte_data(self.VL53L0X_ADDR_DEFAULT, register, value)
        time.sleep(0.1)
    
    def __read(self, register):
        return self.bus.read_byte_data(self.VL53L0X_ADDR_DEFAULT, register)

    def __read_raw_bits(self, addr, register):
        # read values
        high = self.bus.read_byte_data(addr, register)
        low = self.bus.read_byte_data(addr, register+1)

        # combine higha and low for unsigned bit value
        value = ((high << 8) | low)
        
        # convert to +- value
        if(value > 32768):
            value -= 65536
        return value
