import smbus, time
from pca9685 import pca9685

bus = smbus.SMBus(1) # start comm with i2c bus
pca = pca9685(bus)
pca.start()

time.sleep(1) 

pca.write_pwm(4, 2048)

