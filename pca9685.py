import smbus,time

class pca9685:
    def __init__(self, bus):
        # pca9685 Registers
        self.PCA9685_ADDR = 0x40
        self.MOD1 = 0x00

        self.bus = bus

    def start(self):

        # Normal Mode and Allcall responds
        value = 0x01
        self.__write(self.PCA9685_ADDR, self.MOD1, value)


    def write_pwm(self, num_led, value_off):

        if num_led > 15 or num_led < 0:
            print("bad num led")
            return -1

        if value_off > 4095  or value_off < 0:
            print("bad pwm")
            return -2

        addrL_ON  = 0x06 + num_led*4
        addrH_ON  = 0x06 + num_led*4 +1
        addrL_OFF = 0x06 + num_led*4 +2
        addrH_OFF = 0x06 + num_led*4 +3

        self.__write(self.PCA9685_ADDR, addrL_ON, 0x00)
        self.__write(self.PCA9685_ADDR, addrH_ON, 0x00)
        self.__write(self.PCA9685_ADDR, addrL_OFF, value_off&0xFF)
        self.__write(self.PCA9685_ADDR, addrH_OFF, (value_off>>8)&0xF)



    def __write(self, addr, register, value):
        self.bus.write_byte_data(addr, register, value)
        time.sleep(0.1)

    def __read_raw_bits(self, addr, register):
        # read values
        high = self.bus.read_byte_data(addr, register)
        low = self.bus.read_byte_data(addr, register+1)

        # combine higha and low for unsigned bit value
        value = ((high << 8) | low)
        
        # convert to +- value
        if(value > 32768):
            value -= 65536
        return value
