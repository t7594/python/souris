# MPU6050 9-DoF Example Printout

import smbus, time
from mpu9250_i2c import mpu9250
import adafruit_vl53l0x

bus = smbus.SMBus(1) # start comm with i2c bus
mpu = mpu9250(bus)
mpu.start()

time.sleep(1) # delay necessary to allow mpu9250 to settle

print('recording data')
while 1:
    try:
        ax,ay,az,wx,wy,wz = mpu.mpu9250_conv() # read and convert mpu9250 data
        mx,my,mz = mpu.AK8963_conv() # read and convert AK8963 magnetometer data
    except:
        continue
    
    print('{}'.format('-'*30))
    print('accel [g]: x = {0:2.2f}, y = {1:2.2f}, z {2:2.2f}= '.format(ax,ay,az))
    print('gyro [dps]:  x = {0:2.2f}, y = {1:2.2f}, z = {2:2.2f}'.format(wx,wy,wz))
    print('mag [uT]:   x = {0:2.2f}, y = {1:2.2f}, z = {2:2.2f}'.format(mx,my,mz))
    print('{}'.format('-'*30))
    time.sleep(1)
